FROM mcr.microsoft.com/dotnet/sdk:6.0

WORKDIR /app
COPY . .
WORKDIR /app/src/ArmTemplates

RUN dotnet restore

ENTRYPOINT ["dotnet", "run", "create"]
